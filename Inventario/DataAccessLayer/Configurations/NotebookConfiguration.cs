﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    // fluent api - набор правил для Notebook -вызывается в datacontext
    public class NotebookConfiguration : IEntityTypeConfiguration<Notebook>
    {
        public void Configure(EntityTypeBuilder<Notebook> modelBuilder)
        {
            modelBuilder.HasKey(notebook => notebook.ID); // указание на первичный ключ
            /*//modelBuilder.HasOne(n => n.Employee).WithMany(n => n.Notebooks).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Property("Model").HasMaxLength(100).IsRequired();
            modelBuilder.Property("SN").HasMaxLength(100).IsRequired();
            modelBuilder.Property("Status").IsRequired();
            modelBuilder.Property("HasLaptopBag").IsRequired();
            modelBuilder.Property("HasСomputerMouse").IsRequired();
            modelBuilder.Property("DateOfIssue").IsRequired();*/
        }
    }
}

