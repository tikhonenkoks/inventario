﻿using FluentValidation;

namespace Domain.Models.Phones.Commands
{
    /// <summary>
    /// Валидатор CQRS модели удаления телефона
    /// </summary>
    public class DeletePhoneCommandValidator : AbstractValidator<DeletePhoneCommand>
    {
        /// <summary>
        /// Правила валидации CQRS модели удаления телефона
        /// </summary>
        public DeletePhoneCommandValidator()
        {
            RuleFor(command => command.Id)
                .NotNull()
                .GreaterThanOrEqualTo(0)
                .WithMessage("Поле должно быть больше или равно 0");
        }
    }
}
