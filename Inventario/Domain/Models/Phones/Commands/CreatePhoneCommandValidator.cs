﻿using FluentValidation;

namespace Domain.Models.Phones.Commands
{
    /// <summary>
    /// Валидатор CQRS модели создания телефона
    /// </summary>
    public class CreatePhoneCommandValidator : AbstractValidator<PhoneDto>
    {
        /// <summary>
        /// Правила валидации CQRS модели создания телефона
        /// </summary>
        public CreatePhoneCommandValidator()
        {
            RuleFor(phone => phone.ID)
            .NotNull()
            .GreaterThanOrEqualTo(0)
            .WithMessage("Поле должно быть больше или равно 0");

            RuleFor(phone => phone.Model)
            .NotNull()
            .MinimumLength(1)
            .MaximumLength(100)
            .WithMessage("Поле Model должно быть от 1 до 100 символов");

            RuleFor(phone => phone.SN)
            .NotNull()
            .MinimumLength(1)
            .MaximumLength(100)
            .WithMessage("Поле SN должно содержать от 1 и до 100");

            RuleFor(phone => phone.Status)
            .NotNull()
            .WithMessage("Статусы: выдан, готов к выдаче, на списании, списан, в ремонте");

            RuleFor(phone => phone.HasCharger)
            .NotNull()
            .WithMessage("Поле HasCharger должно содержать true или false");

            RuleFor(phone => phone.PhoneNumber)
            .NotNull()
            .MinimumLength(7)
            .MaximumLength(11)
            .WithMessage("Поле PhoneNumber должно содержать от 7 и до 11 цифр");

            RuleFor(phone => phone.DateOfIssue)
            .NotNull()
            .WithMessage("Поле DateOfIssue должно быть в формате даты yyyy.MM.dd");
        }
    }
}


