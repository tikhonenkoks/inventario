﻿using Domain.Models.Employees;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models.Notebooks;
/// <summary>
/// Ноутбук 
/// </summary>
public partial class NotebookDto
{
    /// <summary>
    /// Идентификатор ноутбука
    /// </summary>
    public int ID { get; set; }
    /// <summary>
    /// Модель ноутбука
    /// </summary>
    public string Model { get; set; }
    /// <summary>
    /// Серийный номер 
    /// </summary>
    public string SN { get; set; }
    /// <summary>
    /// Статус оборудования
    /// </summary>
    public Status Status { get; set; }
    /// <summary>
    /// В комплекте с сумкой
    /// </summary>
    public bool HasLaptopBag { get; set; }
    /// <summary>
    /// В комплекте с компьютерной мышкой
    /// </summary>
    public bool HasСomputerMouse { get; set; }
    /// <summary>
    /// Дата выдачи оборудования
    /// </summary>
    public string DateOfIssue { get; set; }

}
