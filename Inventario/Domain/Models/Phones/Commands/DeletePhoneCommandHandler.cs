﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Domain.Exceptions;
using MediatR;

namespace Domain.Models.Phones.Commands
{
    /// <summary>
    /// CQRS обработчик удаления телефона
    /// </summary>
    public class DeletePhoneCommandHandler : IRequestHandler<DeletePhoneCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public DeletePhoneCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды удаления телефона
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор удаляемого телефона</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого телефона нет</exception>
        public async Task<int> Handle(DeletePhoneCommand request, CancellationToken cancellationToken)
        {
            var phone = _dataContext.Phones
            .FirstOrDefault(phone1 => phone1.ID == request.Id);
            if (phone != null)
            {
                _dataContext.Phones.Remove(phone);
                await _dataContext.SaveChangesAsync(cancellationToken);
                return phone.ID;
            }
            throw new NotFoundException($"Телефон с Id: {request.Id} не найден");
        }
    }
}
