﻿using Domain.Models.Modems;
using Domain.Models.Modems.Commands;
using Domain.Models.Modems.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Controllers;

[Authorize]
/// <summary>
/// Контроллер для ModemDto
/// </summary>
[ApiController]
[Route("api/[controller]/[action]")]
public class ModemController : ControllerBase
{
    private readonly IMediator _mediator;

    /// <summary>
    /// Конструктор для контроллера
    /// </summary>
    /// <param name="mediator">медиатор для работы с CQRS</param>
    public ModemController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Получение модема по id 
    /// </summary>
    /// <param name="id">Идентификатор модема</param>
    /// <returns>Модем</returns>
    [Authorize(Roles = "Admin, User")]
    [HttpGet("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(ModemDto), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Get(int id)
    {
        var getModemQuery = new GetModemQuery() { Id = id };
        var modemDto = await _mediator.Send(getModemQuery);
        return Ok(modemDto);
    }

    /// <summary>
    /// Получение модемов
    /// </summary>
    /// <param name="getModemsQuery"> пагинация</param>
    /// <returns> Результирующий набор модемов </returns>
    [Authorize(Roles = "Admin, User")]
    [HttpGet]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(IEnumerable<ModemDto>), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult<IEnumerable<ModemDto>>> Get([FromQuery] GetModemsQuery getModemsQuery)
    {
        var modemsDto = await _mediator.Send(getModemsQuery);
        return Ok(modemsDto);
    }

    /// <summary>
    /// Создание модема
    /// </summary>
    /// <param name="createModemCommand">Экземпляр команды CQRS CreateModemCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Create([FromBody] CreateModemCommand createModemCommand)
    {
        await _mediator.Send(createModemCommand);
        return Ok();
    }

    /// <summary>
    /// Удаление модема по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации
    public async Task<ActionResult> Delete([FromRoute] int id)
    {
        var modem = new DeleteModemCommand() { Id = id };
        await _mediator.Send(modem);
        return Ok(modem);
    }

    /// <summary>
    /// Изменение модема по идентификатору
    /// </summary>
    /// <param name="id">Bдентификатор</param>
    /// <param name="editModemCommand"></param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPut("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации
    public async Task<ActionResult> Edit([FromRoute] int id, [FromBody] EditModemCommand editModemCommand)
    {

        var modem = new EditModemCommand() { ID = id };
        await _mediator.Send(editModemCommand);
        return Ok(modem);

        /*var modem = await _mediator.Send(editModemCommand);
        return Ok(modem);*/
    }

    
}