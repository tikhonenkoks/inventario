﻿using AutoMapper;
using DataAccessLayer.Models;
using Domain.Models.Phones.Commands;

namespace Domain.Models.Phones;

/// <summary>
/// Автомаппер CQRS модели получения телефона(-ов) и моделей в базе данных
/// </summary>
public class PhoneMapConfig : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели получения телефона(-ов) и моделей в базе данных
    /// </summary>
    public PhoneMapConfig() // конфиг для автомаппера. настройка сопоставления полей моделей
    {
        CreateMap<PhoneDto, Phone>()
            .ForMember(phone => phone.ID, expression => expression.MapFrom(dto => dto.ID))
            .ForMember(phone => phone.Model, expression => expression.MapFrom(dto => dto.Model))
            .ForMember(phone => phone.SN, expression => expression.MapFrom(dto => dto.SN))
            .ForMember(phone => phone.Status, expression => expression.MapFrom(dto => dto.Status))
            .ForMember(phone => phone.HasCharger, expression => expression.MapFrom(dto => dto.HasCharger))
            .ForMember(phone => phone.PhoneNumber, expression => expression.MapFrom(dto => dto.PhoneNumber))
            .ForMember(phone => phone.DateOfIssue, expression => expression.MapFrom(dto => dto.DateOfIssue))
            .ReverseMap();

        CreateMap<CreatePhoneCommand, Phone>()
            .ForMember(phone => phone.ID, expression => expression.MapFrom(create => create.ID))
            .ForMember(phone => phone.Model, expression => expression.MapFrom(create => create.Model))
            .ForMember(phone => phone.SN, expression => expression.MapFrom(create => create.SN))
            .ForMember(phone => phone.Status, expression => expression.MapFrom(create => create.Status))
            .ForMember(phone => phone.HasCharger, expression => expression.MapFrom(create => create.HasCharger))
            .ForMember(phone => phone.PhoneNumber, expression => expression.MapFrom(create => create.PhoneNumber))
            .ForMember(phone => phone.DateOfIssue, expression => expression.MapFrom(create => create.DateOfIssue))
            .ReverseMap();

        CreateMap<DeletePhoneCommand, Phone>()
            .ForMember(phone => phone.ID, expression => expression.MapFrom(delete => delete.Id))
            .ReverseMap();

        CreateMap<EditPhoneCommand, Phone>()
            .ForMember(phone => phone.ID, expression => expression.MapFrom(edit => edit.ID))
            .ForMember(phone => phone.Model, expression => expression.MapFrom(edit => edit.Model))
            .ForMember(phone => phone.SN, expression => expression.MapFrom(edit => edit.SN))
            .ForMember(phone => phone.Status, expression => expression.MapFrom(edit => edit.Status))
            .ForMember(phone => phone.HasCharger, expression => expression.MapFrom(edit => edit.HasCharger))
            .ForMember(phone => phone.PhoneNumber, expression => expression.MapFrom(edit => edit.PhoneNumber))
            .ForMember(phone => phone.DateOfIssue, expression => expression.MapFrom(edit => edit.DateOfIssue))
            .ReverseMap();

    }
}
