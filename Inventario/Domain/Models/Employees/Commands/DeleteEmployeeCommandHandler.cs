﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Domain.Exceptions;
using Domain.Models.Employees.Commands;
using MediatR;

namespace Domain.Users.Commands
{
    /// <summary>
    /// CQRS обработчик удаления сотрудника
    /// </summary>
    public class DeleteEmployeeCommandHandler : IRequestHandler<DeleteEmployeeCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public DeleteEmployeeCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды удаления сотрудника
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор удаляемого сотрудника</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого сотрудника нет</exception>
        public async Task<int> Handle(DeleteEmployeeCommand request, CancellationToken cancellationToken)
        {
            var employee = _dataContext.Employees
            .FirstOrDefault(employee1 => employee1.ID == request.Id);
            if (employee != null)
            {
                _dataContext.Employees.Remove(employee);
                await _dataContext.SaveChangesAsync(cancellationToken);
                return employee.ID;
            }
            throw new NotFoundException($"Cотрудник с Id: {request.Id} не найден");
        }
    }
}
