﻿using Domain.Models.Phones;
using Domain.Models.Phones.Commands;
using Domain.Models.Phones.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Controllers;

[Authorize]
/// <summary>
/// Контроллер для PhoneDto
/// </summary>
[ApiController]
[Route("api/[controller]/[action]")]
public class PhoneController : ControllerBase
{
    private readonly IMediator _mediator;

    /// <summary>
    /// Конструктор для контроллера
    /// </summary>
    /// <param name="mediator">медиатор для работы с CQRS</param>
    public PhoneController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Получение телефона по id 
    /// </summary>
    /// <param name="id">Идентификатор телефона</param>
    /// <returns>Телефон</returns>
    [Authorize(Roles = "Admin, User")]
    [HttpGet("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(PhoneDto), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Get(int id)
    {
        var getPhoneQuery = new GetPhoneQuery() { Id = id };
        var phoneDto = await _mediator.Send(getPhoneQuery);
        return Ok(phoneDto);
    }

    /// <summary>
    /// Получение телефонов
    /// </summary>
    /// <param name="getPhonesQuery"> пагинация</param>
    /// <returns> Результирующий набор билетов </returns>
    [Authorize(Roles = "Admin, User")]
    [HttpGet]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(IEnumerable<PhoneDto>), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult<IEnumerable<PhoneDto>>> Get([FromQuery] GetPhonesQuery getPhonesQuery)
    {
        var phonesDto = await _mediator.Send(getPhonesQuery);
        return Ok(phonesDto);
    }

    /// <summary>
    /// Создание телефона
    /// </summary>
    /// <param name="createPhoneCommand">Экземпляр команды CQRS CreatePhoneCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Create([FromBody] CreatePhoneCommand createPhoneCommand)
    {
        await _mediator.Send(createPhoneCommand);
        return Ok();
    }

    /// <summary>
    /// Удаление телефона по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации
    public async Task<ActionResult> Delete([FromRoute] int id)
    {
        var phoneDto = new DeletePhoneCommand() { Id = id };
        await _mediator.Send(phoneDto);
        return Ok(phoneDto);
    }

    /// <summary>
    /// Изменение телефона
    /// </summary>
    /// <param name="editPhoneCommand">Экземпляр команды CQRS EditPhoneCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPut("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации
    public async Task<ActionResult> Edit([FromBody] EditPhoneCommand editPhoneCommand)
    {
        var phoneDto = await _mediator.Send(editPhoneCommand);
        return Ok(phoneDto);
    }
}