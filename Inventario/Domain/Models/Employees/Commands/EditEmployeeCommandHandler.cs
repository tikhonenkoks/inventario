﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Employees.Commands
{
    /// <summary>
    /// CQRS обработчик редактирования сотрудника 
    /// </summary>
    public class EditEmployeeCommandHandler : IRequestHandler<EditEmployeeCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public EditEmployeeCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды редактирования сотрудника
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор сотрудника</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого сотрудника нет</exception>
        public async Task<int> Handle(EditEmployeeCommand request, CancellationToken cancellationToken)
        {
            var employee = await _dataContext.Employees
            .FirstOrDefaultAsync(employee1 => employee1.ID == request.ID, cancellationToken: cancellationToken);

            if (employee == null)
                throw new NotFoundException($"Cотрудник с ID: {request.ID} не найден");

            _mapper.Map(request, employee);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return employee.ID;
        }
    }
}

