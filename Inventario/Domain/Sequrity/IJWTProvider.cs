﻿
namespace Domain.Sequrity
{
    /// <summary>
    /// Создание JWT для авторизации
    /// </summary>
    public interface IJWTProvider
    {
        string GetToken (string login, string role, int id);
    }
}
