﻿using MediatR;

namespace Domain.Models.Phones.Queries;

/// <summary>
/// CQRS модель получения телефонов пагинацией
/// </summary>
public class GetPhonesQuery : IRequest<IEnumerable<PhoneDto>>
{
    /// <summary>
    /// Пропуск телефонов с начала списка
    /// </summary>
    public int Skip { get; set; }
    /// <summary>
    /// Количество требуемых телефонов 
    /// </summary>
    public int Take { get; set; }
}