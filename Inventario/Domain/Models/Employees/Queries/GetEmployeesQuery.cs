﻿using MediatR;

namespace Domain.Models.Employees.Queries;

/// <summary>
/// CQRS модель получения всех сотрудника
/// </summary>
public class GetEmployeesQuery : IRequest<IEnumerable<EmployeeDto>>
{
}