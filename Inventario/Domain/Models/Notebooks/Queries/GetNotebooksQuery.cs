﻿using MediatR;

namespace Domain.Models.Notebooks.Queries;

/// <summary>
/// CQRS модель получения ноутбуков пагинацией
/// </summary>
public class GetNotebooksQuery : IRequest<IEnumerable<NotebookDto>>
{
    /// <summary>
    /// Пропуск ноутбуков с начала списка
    /// </summary>
    public int Skip { get; set; }
    /// <summary>
    /// Количество требуемых ноутбуков 
    /// </summary>
    public int Take { get; set; }
}