﻿namespace Domain
{
    /// <summary>
    /// Свойство Secret
    /// </summary>
    public class DomainOptions
    {
        public string Secret { get; set; }
    }
}
