using DataAccessLayer;
using Domain;
using Domain.Models.Employees;
using Domain.Models.Employees.Commands;
using Domain.Models.Employees.Queries;
using Domain.Models.Modems;
using Domain.Models.Modems.Commands;
using Domain.Models.Modems.Queries;
using Domain.Models.Notebooks;
using Domain.Models.Notebooks.Commands;
using Domain.Models.Notebooks.Queries;
using Domain.Models.Phones;
using Domain.Models.Phones.Commands;
using Domain.Models.Phones.Queries;
using Domain.Models.Users;
using Domain.Sequrity;
using Domain.Users.Commands;
using Domain.Users.Queries;
using Extensions;
using FluentValidation;
using FluentValidation.AspNetCore;
using Inventario_API;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Middlewares;
using Serilog;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((context, configuration) => //��������� �����������
{
    configuration.MinimumLevel.Debug();
    configuration.WriteTo.Console();
    configuration.WriteTo.File("log.txt");
});

var applicationOptions = builder.Configuration.Get<ApplicationOptions>();

builder.Services.Configure<DomainOptions>(builder.Configuration); // ���������� ������������ � ���������
builder.Services.Configure<ApplicationOptions>(builder.Configuration); // ���������� ������������ � ���������

builder.Services.AddDbContext<DataContext>(optionsBuilder => optionsBuilder.UseNpgsql(applicationOptions.DbConnection));
builder.Services.AddDatabaseDeveloperPageExceptionFilter(); // Microsoft.AspNetCore.Diagnostics.EntityFrameworkCore ����� ��� ������ � ������������ � ��!

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    {
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Inventario-API.xml"));
    }
    {
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Domain.xml"));
    }
    {
        options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
            In = ParameterLocation.Header,
            Description = "Please insert JWT with Bearer into field",
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey,
            BearerFormat = "JWT"
        });
    }    
    options.AddSecurityRequirement(new OpenApiSecurityRequirement()
         {
             {
                 new OpenApiSecurityScheme
                 {
                     Reference = new OpenApiReference {Type = ReferenceType.SecurityScheme, Id = "Bearer"}
                 },
                 new string[] { }
             }
         });

    options.CustomSchemaIds(y => y.FullName);
});

builder.Services.AddTransient<IJWTProvider, JWTProvider>();// ���������� ���������� ��� ������ � �������� JWT

builder.Services.AddFluentValidationAutoValidation(); // ��������� �������������� ��������� Fluent Validation

//���������� ����������� CQRS
builder.Services.AddValidatorsFromAssemblyContaining<CreateEmployeeCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<DeleteEmployeeCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<EditEmployeeCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<GetEmployeeQueryValidator>();  

builder.Services.AddValidatorsFromAssemblyContaining<CreateModemCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<DeleteModemCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<EditModemCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<GetModemQueryValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<GetModemsQueryValidator>();  

builder.Services.AddValidatorsFromAssemblyContaining<CreateNotebookCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<DeleteNotebookCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<EditNotebookCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<GetNotebookQueryValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<GetNotebooksQueryValidator>();  

builder.Services.AddValidatorsFromAssemblyContaining<CreatePhoneCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<DeletePhoneCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<EditPhoneCommandValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<GetPhoneQueryValidator>();  
builder.Services.AddValidatorsFromAssemblyContaining<GetPhonesQueryValidator>();  

builder.Services.AddValidatorsFromAssemblyContaining<CreateUserCommandValidator>(); 
builder.Services.AddValidatorsFromAssemblyContaining<DeleteUserCommandValidator>(); 
builder.Services.AddValidatorsFromAssemblyContaining<EditUserCommandValidator>(); 
builder.Services.AddValidatorsFromAssemblyContaining<GetUserQueryValidator>();

// ���������� �����������
builder.Services.AddAutoMapper(
    typeof(EmployeeMapConfig), typeof(ModemMapConfig),
    typeof(NotebookMapConfig), typeof(PhoneMapConfig),
    typeof(UserMapConfig));

// ���������� ���������
builder.Services.AddMediatR(
    typeof(CreateEmployeeCommand), typeof(CreateModemCommand), typeof(CreateNotebookCommand), typeof(CreatePhoneCommand), typeof(CreateUserCommand),
    typeof(DeleteEmployeeCommand), typeof(DeleteModemCommand), typeof(DeleteNotebookCommand), typeof(DeletePhoneCommand), typeof(DeleteUserCommand),
    typeof(EditEmployeeCommand), typeof(EditModemCommand), typeof(EditNotebookCommand), typeof(EditPhoneCommand), typeof(EditUserCommand),
    typeof(GetEmployeeQuery), typeof(GetModemQuery), typeof(GetNotebookQuery), typeof(GetPhoneQuery), typeof(GetUserQuery),
    typeof(GetEmployeesQuery), typeof(GetModemsQuery), typeof(GetNotebooksQuery), typeof(GetPhonesQuery), typeof(GetUsersQuery));

builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.RequireAuthenticatedSignIn = false;
})
     .AddJwtBearer(options =>
     {
         options.RequireHttpsMetadata = false;
         options.SaveToken = true;
         options.TokenValidationParameters = new TokenValidationParameters
         {
             ValidateIssuerSigningKey = true,
             IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(applicationOptions.Secret)),
             ValidateIssuer = false,
             ValidateAudience = false
         };
     });

builder.Services.AddAuthorization();

var app = builder.Build();

app.UseExceptionsHandlerCustomMiddleware(); //���������� ��������� ��������� ����������
app.UseLogUrl(); //���������� �����������

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();
app.Run();
