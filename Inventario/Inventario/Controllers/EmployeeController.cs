﻿using Domain.Models.Employees;
using Domain.Models.Employees.Commands;
using Domain.Models.Employees.Queries;
using Domain.Models.Users;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace Controllers;

[Authorize]
/// <summary>
/// Контроллер для EmployeeDto
/// </summary>
[ApiController]
[Route("api/[controller]")]

public class EmployeeController : ControllerBase
{
    private readonly IMediator _mediator;

    /// <summary>
    /// Конструктор для контроллера
    /// </summary>
    /// <param name="mediator">Экземпляр mediator</param>
    public EmployeeController(IMediator mediator) // в конструкторе прописана зависимость
    {
        _mediator = mediator;
    }

    [Authorize(Roles = "Admin, User")]
    /// <summary>
    /// Получение пользователя по id
    /// </summary>
    /// <param name="id">Идентификатор пользователя</param>
    /// <returns>Пользователь</returns>
    [HttpGet("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(EmployeeDto), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта

    public async Task<ActionResult> Get(int id)
    {
        var getEmployeeQuery = new GetEmployeeQuery() { Id = id };
        var employeeDto = await _mediator.Send(getEmployeeQuery);
        return Ok(employeeDto);
    }

    [Authorize(Roles = "Admin")]
    /// <summary>
    /// Получение всех пользователей
    /// </summary>
    /// <returns>Результирующий набор пользователей</returns>
    [HttpGet]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(IEnumerable<UserDto>), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult<IEnumerable<EmployeeDto>>> GetAll([FromQuery] GetEmployeesQuery getEmployeesQuery)
    {
        var employeesDto = await _mediator.Send(getEmployeesQuery);
        return Ok(employeesDto);
    }

    /// <summary>
    /// Создание пользователя
    /// </summary>
    /// <param name="createEmployeeCommand">Экземпляр CreateEmployeeCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Create([FromBody] CreateEmployeeCommand createEmployeeCommand)
    {
        await _mediator.Send(createEmployeeCommand);
        return Ok();
    }

    /// <summary>
    /// Удаление пользователя по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации
    public async Task<ActionResult> Delete([FromRoute] int id)
    {
        var employee = new DeleteEmployeeCommand() { Id = id };
        await _mediator.Send(employee);
        return Ok(employee);
    }

    /// <summary>
    /// Изменение пользователя по идентификатору
    /// </summary>
    /// <param name="editEmployeeCommand">Экземпляр EditEmployeeCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPut("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Edit([FromBody] EditEmployeeCommand editEmployeeCommand)
    {
        var employee = await _mediator.Send(editEmployeeCommand);
        return Ok(employee);
    }
}

