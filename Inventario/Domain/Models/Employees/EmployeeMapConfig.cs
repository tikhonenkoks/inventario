﻿using AutoMapper;
using DataAccessLayer.Models;
using Domain.Models.Employees.Commands;

namespace Domain.Models.Employees
{
    /// <summary>
    /// Автомаппер сотрудника и модели в базе данных
    /// </summary>
    public class EmployeeMapConfig : Profile
    {
        /// <summary>
        /// Правила маппинга сотрудника и модели в базе данных
        /// </summary>
        public EmployeeMapConfig()
        {
            CreateMap<Employee, EmployeeDto>()
                .ForMember(dto => dto.ID, expression => expression.MapFrom(employee => employee.ID))
                .ForMember(dto => dto.FirstName, expression => expression.MapFrom(employee => employee.FirstName))
                .ForMember(dto => dto.LastName, expression => expression.MapFrom(employee => employee.LastName))
                .ForMember(dto => dto.Department, expression => expression.MapFrom(employee => employee.Department))
                .ForMember(dto => dto.ModemsDto, expression => expression.MapFrom(employee => employee.Modems))
                .ForMember(dto => dto.NotebooksDto, expression => expression.MapFrom(employee => employee.Notebooks))
                .ForMember(dto => dto.PhonesDto, expression => expression.MapFrom(employee => employee.Phones))
                .ReverseMap();

            CreateMap<CreateEmployeeCommand, Employee>()
                .ForMember(employee => employee.ID, expression => expression.MapFrom(create => create.ID))
                .ForMember(employee => employee.FirstName, expression => expression.MapFrom(create => create.FirstName))
                .ForMember(employee => employee.LastName, expression => expression.MapFrom(create => create.LastName))
                .ForMember(employee => employee.Department, expression => expression.MapFrom(create => create.Department))
                .ReverseMap();

            CreateMap<DeleteEmployeeCommand, Employee>()
                .ForMember(employee => employee.ID, expression => expression.MapFrom(delete => delete.Id))
                .ReverseMap();

            CreateMap<EditEmployeeCommand, Employee>()
                .ForMember(employee => employee.ID, expression => expression.MapFrom(edit => edit.ID))
                .ForMember(employee => employee.FirstName, expression => expression.MapFrom(edit => edit.FirstName))
                .ForMember(employee => employee.LastName, expression => expression.MapFrom(edit => edit.LastName))
                .ForMember(employee => employee.Department, expression => expression.MapFrom(edit => edit.Department))
                .ReverseMap();
        }
    }
}
