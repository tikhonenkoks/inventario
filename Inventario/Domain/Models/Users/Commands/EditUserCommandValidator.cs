﻿using Domain.Models.Users;
using FluentValidation;

namespace Domain.Users.Commands
{
    /// <summary>
    /// Валидатор CQRS модели редактирования пользователя
    /// </summary>
    public class EditUserCommandValidator : AbstractValidator<UserDto>
    {
        /// <summary>
        /// Правила валидации CQRS модели редактирования пользователя
        /// </summary>
        public EditUserCommandValidator()
        {
            RuleFor(user => user.ID)
                .NotNull()
                .NotEmpty()
                .GreaterThanOrEqualTo(0)
                .WithMessage("Поле должно быть больше или равно 0");

            RuleFor(user => user.Login)
                .NotNull()
                .NotEmpty()
                .MinimumLength(3)
                .MaximumLength(20)
                .WithMessage("Поле Login должно содержать от 3 до 20 символов");

            RuleFor(user => user.Password)
                .NotNull()
                .NotEmpty()
                .MinimumLength(8)
                .MaximumLength(50)
                .WithMessage("Поле Password должно содержать от 8 до 50 символов");

            RuleFor(user => user.Role)
                .NotNull()
                .IsInEnum();
        }
    }
}

