﻿using AutoMapper;
using DataAccessLayer.Models;
using Domain.Models.Modems.Commands;

namespace Domain.Models.Modems;

/// <summary>
/// Автомаппер CQRS модели получения модема(-ов) и моделей в базе данных
/// </summary>
public class ModemMapConfig : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели получения модема(-ов) и моделей в базе данных
    /// </summary>
    public ModemMapConfig() // конфиг для автомаппера. настройка сопоставления полей моделей
    {
        CreateMap<ModemDto, Modem>()
            .ForMember(modem => modem.ID, expression => expression.MapFrom(dto => dto.ID))
            .ForMember(modem => modem.Model, expression => expression.MapFrom(dto => dto.Model))
            .ForMember(modem => modem.SN, expression => expression.MapFrom(dto => dto.SN))
            .ForMember(modem => modem.Status, expression => expression.MapFrom(dto => dto.Status))
            .ForMember(modem => modem.PhoneNumber, expression => expression.MapFrom(dto => dto.PhoneNumber))
            .ForMember(modem => modem.DateOfIssue, expression => expression.MapFrom(dto => dto.DateOfIssue))
            //.ForMember(modem => modem.EmployeeID, expression => expression.MapFrom(dto => dto.EmployeeDtoID))
            .ReverseMap();

        CreateMap<CreateModemCommand, Modem>()
            .ForMember(modem => modem.ID, expression => expression.MapFrom(create => create.ID))
            .ForMember(modem => modem.Model, expression => expression.MapFrom(create => create.Model))
            .ForMember(modem => modem.SN, expression => expression.MapFrom(create => create.SN))
            .ForMember(modem => modem.Status, expression => expression.MapFrom(create => create.Status))
            .ForMember(modem => modem.PhoneNumber, expression => expression.MapFrom(create => create.PhoneNumber))
            .ForMember(modem => modem.DateOfIssue, expression => expression.MapFrom(create => create.DateOfIssue))
            .ReverseMap();

        CreateMap<DeleteModemCommand, Modem>()
            .ForMember(modem => modem.ID, expression => expression.MapFrom(delete => delete.Id))
            .ReverseMap();

        CreateMap<EditModemCommand, Modem>()
            .ForMember(modem => modem.ID, expression => expression.MapFrom(edit => edit.ID))
            .ForMember(modem => modem.Model, expression => expression.MapFrom(edit => edit.Model))
            .ForMember(modem => modem.SN, expression => expression.MapFrom(edit => edit.SN))
            .ForMember(modem => modem.Status, expression => expression.MapFrom(edit => edit.Status))
            .ForMember(modem => modem.PhoneNumber, expression => expression.MapFrom(edit => edit.PhoneNumber))
            .ForMember(modem => modem.DateOfIssue, expression => expression.MapFrom(edit => edit.DateOfIssue))
            //.ForMember(modem => modem.EmployeeID, expression => expression.MapFrom(dto => dto.EmployeeDtoID))
            .ReverseMap();
    }
}
