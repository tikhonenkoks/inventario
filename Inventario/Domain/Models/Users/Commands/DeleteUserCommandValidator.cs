﻿using Domain.Users.Commands;
using FluentValidation;

/// <summary>
/// Валидатор CQRS модели удаления пользователя
/// </summary>
public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
{
    /// <summary>
    /// Правила валидации CQRS модели удаления пользователя
    /// </summary>
    public DeleteUserCommandValidator()
    {
        RuleFor(command => command.Id)
            .NotNull()
            .NotEmpty()
            .GreaterThanOrEqualTo(0)
            .WithMessage("Поле должно быть больше или равно 0");
    }
}