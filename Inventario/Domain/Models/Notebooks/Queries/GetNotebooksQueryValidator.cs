﻿using FluentValidation;

namespace Domain.Models.Notebooks.Queries;

/// <summary>
/// Валидатор CQRS модели получения ноутбуков
/// </summary>
public class GetNotebooksQueryValidator : AbstractValidator<GetNotebooksQuery>
{
    /// <summary>
    /// Правила валидации CQRS модели получения ноутбуков
    /// </summary>
    public GetNotebooksQueryValidator()
    {
        RuleFor(query => query.Skip).GreaterThanOrEqualTo(0);
        RuleFor(query => query.Take).GreaterThanOrEqualTo(0);

        RuleFor(query => query.Skip).Must((query, skip) => skip <= query.Take);
    }
}
