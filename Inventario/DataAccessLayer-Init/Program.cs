﻿using DataAccessLayer;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using System.Globalization;

var optionsBuilder = new DbContextOptionsBuilder<DataContext>().UseNpgsql("Host=localhost;Port=5433;Database=Inventario;Username=postgres;Password=Shemas14!");
var dataContext = new DataContext(optionsBuilder.Options);

dataContext.Users.Add(new DataAccessLayer.Models.User()
{
    Login = "admin",
    Password = "admin",
    Role = DataAccessLayer.Models.Role.Admin
});

dataContext.Users.Add(new DataAccessLayer.Models.User()
{
    Login = "user",
    Password = "user",
    Role = DataAccessLayer.Models.Role.User
});



dataContext.Employees.Add(new DataAccessLayer.Models.Employee()
{
    FirstName = "Сергей",
    LastName = "Гончаров",
    Department = "УЗЗ"
});

dataContext.Employees.Add(new DataAccessLayer.Models.Employee()
{
    FirstName = "Ирина",
    LastName = "Лялина",
    Department = "Бухгалтерия"
});

dataContext.Employees.Add(new DataAccessLayer.Models.Employee()
{
    FirstName = "Кирилл",
    LastName = "Тихоненко",
    Department = "ОАБП"
});



dataContext.Phones.Add(new DataAccessLayer.Models.Phone()
{
    Model = "Huawei P50",
    SN = "GDKJ7wr87",
    Status = DataAccessLayer.Models.Status.HasUser,
    HasCharger = true,
    PhoneNumber = "79056375463",
    DateOfIssue = "2023.01.01",
    //EmployeeID = 1
});

/*dataContext.Phones.Add(new DataAccessLayer.Models.Phone()
{
    Model = "Huawei P200",
    SN = "OHIULHG23432423",
    Status = DataAccessLayer.Models.Status.HasUser,
    HasCharger = true,
    PhoneNumber = "79334358934",
    DateOfIssue = "2023.01.02",
    //EmployeeID = 1
});*/



dataContext.Modems.Add(new DataAccessLayer.Models.Modem()
{
    Model = "Huawei e3372h",
    SN = "GTG4234JJ",
    Status = DataAccessLayer.Models.Status.HasUser,
    PhoneNumber = "79340984567",
    DateOfIssue = "2023.01.03",
    //EmployeeID = 1
});

dataContext.Modems.Add(new DataAccessLayer.Models.Modem()
{
    Model = "Huawei e3372h",
    SN = "LKJUI709JK",
    Status = DataAccessLayer.Models.Status.HasUser,
    PhoneNumber = "79345545854",
    DateOfIssue = "2023.01.04"
});



dataContext.Notebooks.Add(new DataAccessLayer.Models.Notebook()
{
    Model = "HIPER Workbook N15RP",
    SN = "KJBH87987",
    Status = DataAccessLayer.Models.Status.HasUser,
    HasLaptopBag = true,
    HasСomputerMouse = true,
    DateOfIssue = "2023.01.05",
    //EmployeeID = 1
});
/*
dataContext.Notebooks.Add(new DataAccessLayer.Models.Notebook()
{
    Model = "Ноутбук HP 255 G8",
    SN = "GTDT7987HIH",
    Status = DataAccessLayer.Models.Status.HasUser,
    HasLaptopBag = true,
    HasСomputerMouse = true,
    DateOfIssue = "2023.01.06",
    //EmployeeID = 1
});

dataContext.Notebooks.Add(new DataAccessLayer.Models.Notebook()
{
    Model = "Ноутбук HP 255 G8",
    SN = "FTYYT79885H",
    Status = DataAccessLayer.Models.Status.InDepartment,
    HasLaptopBag = true,
    HasСomputerMouse = true,
    DateOfIssue = "2023.01.01",
    //EmployeeID = 1
});*/

dataContext.SaveChanges();
Console.ReadKey();
