﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Modems.Commands
{
    /// <summary>
    /// CQRS обработчик редактирования модема 
    /// </summary>
    public class EditModemCommandHandler : IRequestHandler<EditModemCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public EditModemCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды редактирования модема
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор модема</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого модема нет</exception>
        public async Task<int> Handle(EditModemCommand request, CancellationToken cancellationToken)
        {
            var modem = await _dataContext.Modems
            .FirstOrDefaultAsync(modem1 => modem1.ID == request.ID, cancellationToken: cancellationToken);

            if (modem == null)
                throw new NotFoundException($"Модем с ID: {request.ID} не найден");

            _mapper.Map(request, modem);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return modem.ID;
        }
    }
}

