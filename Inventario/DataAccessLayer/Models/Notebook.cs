﻿using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.Models
{
    public class Notebook
    {
        public int ID { get; set; }
        public string Model { get; set; }
        public string SN { get; set; }
        public Status Status { get; set; }
        public bool HasLaptopBag { get; set; }
        public bool HasСomputerMouse { get; set; }
        public string DateOfIssue { get; set; }

        public int EmployeeID { get; set; } // внешний ключ
        public Employee Employee { get; set; }
    }
}
