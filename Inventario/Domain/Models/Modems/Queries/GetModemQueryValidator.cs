﻿using FluentValidation;

namespace Domain.Models.Modems.Queries
{
    /// <summary>
    /// Валидатор CQRS модели получения модема
    /// </summary>
    public class GetModemQueryValidator : AbstractValidator<GetModemQuery>
    {
        /// <summary>
        /// Правила валидации CQRS модели получения модема
        /// </summary>
        public GetModemQueryValidator()
        {
            RuleFor(query => query.Id).GreaterThanOrEqualTo(0);
        }
    }
}
    