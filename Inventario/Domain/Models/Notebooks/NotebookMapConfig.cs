﻿using AutoMapper;
using DataAccessLayer.Models;
using Domain.Models.Notebooks.Commands;

namespace Domain.Models.Notebooks;

/// <summary>
/// Автомаппер CQRS модели получения ноутбука(-ов) и моделей в базе данных
/// </summary>
public class NotebookMapConfig : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели получения ноутбука(-ов) и моделей в базе данных
    /// </summary>
    public NotebookMapConfig() // конфиг для автомаппера. настройка сопоставления полей моделей
    {
        CreateMap<NotebookDto, Notebook>()
            .ForMember(dto => dto.ID, expression => expression.MapFrom(notebook => notebook.ID))
            .ForMember(dto => dto.Model, expression => expression.MapFrom(notebook => notebook.Model))
            .ForMember(dto => dto.SN, expression => expression.MapFrom(notebook => notebook.SN))
            .ForMember(dto => dto.Status, expression => expression.MapFrom(notebook => notebook.Status))
            .ForMember(dto => dto.HasLaptopBag, expression => expression.MapFrom(notebook => notebook.HasLaptopBag))
            .ForMember(dto => dto.HasСomputerMouse, expression => expression.MapFrom(notebook => notebook.HasСomputerMouse))
            .ForMember(dto => dto.DateOfIssue, expression => expression.MapFrom(notebook => notebook.DateOfIssue))
            .ReverseMap();

        CreateMap<CreateNotebookCommand, Notebook>()
            .ForMember(notebook => notebook.ID, expression => expression.MapFrom(create => create.ID))
            .ForMember(notebook => notebook.Model, expression => expression.MapFrom(create => create.Model))
            .ForMember(notebook => notebook.SN, expression => expression.MapFrom(create => create.SN))
            .ForMember(notebook => notebook.Status, expression => expression.MapFrom(create => create.Status))
            .ForMember(notebook => notebook.HasLaptopBag, expression => expression.MapFrom(create => create.HasLaptopBag))
            .ForMember(notebook => notebook.HasСomputerMouse, expression => expression.MapFrom(create => create.HasСomputerMouse))
            .ForMember(notebook => notebook.DateOfIssue, expression => expression.MapFrom(create => create.DateOfIssue))
            .ReverseMap();

        CreateMap<DeleteNotebookCommand, Notebook>()
            .ForMember(notebook => notebook.ID, expression => expression.MapFrom(delete => delete.Id))
            .ReverseMap();

        CreateMap<EditNotebookCommand, Notebook>()
            .ForMember(notebook => notebook.ID, expression => expression.MapFrom(edit => edit.ID))
            .ForMember(notebook => notebook.Model, expression => expression.MapFrom(edit => edit.Model))
            .ForMember(notebook => notebook.SN, expression => expression.MapFrom(edit => edit.SN))
            .ForMember(notebook => notebook.Status, expression => expression.MapFrom(edit => edit.Status))
            .ForMember(notebook => notebook.HasLaptopBag, expression => expression.MapFrom(edit => edit.HasLaptopBag))
            .ForMember(notebook => notebook.HasСomputerMouse, expression => expression.MapFrom(edit => edit.HasСomputerMouse))
            .ForMember(notebook => notebook.DateOfIssue, expression => expression.MapFrom(edit => edit.DateOfIssue))
            .ReverseMap();
    }
}
