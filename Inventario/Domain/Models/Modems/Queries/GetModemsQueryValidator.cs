﻿using FluentValidation;

namespace Domain.Models.Modems.Queries;

/// <summary>
/// Валидатор CQRS модели получения модемов
/// </summary>
public class GetModemsQueryValidator : AbstractValidator<GetModemsQuery>
{
    /// <summary>
    /// Правила валидации CQRS модели получения модемов
    /// </summary>
    public GetModemsQueryValidator()
    {
        RuleFor(query => query.Skip)
            .GreaterThanOrEqualTo(0);
        RuleFor(query => query.Take)
            .GreaterThanOrEqualTo(0);

        RuleFor(query => query.Skip).Must((query, skip) => skip <= query.Take);
    }
}
