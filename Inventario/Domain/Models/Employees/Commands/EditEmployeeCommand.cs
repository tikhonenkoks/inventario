﻿using Domain.Models.Modems;
using Domain.Models.Notebooks;
using Domain.Models.Phones;
using MediatR;


namespace Domain.Models.Employees.Commands
{
    /// <summary>
    /// CQRS модель редактирования сотрудника
    /// </summary>
    public class EditEmployeeCommand : IRequest<int>
    {
       /// <summary>
        /// Идентификатор сотрудника
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Отдел
        /// </summary>
        public string Department { get; set; }

        /*/// <summary>
        /// навигационное свойство
        /// </summary>
        public ICollection<ModemDto>? ModemsDto { get; set; }
        /// <summary>
        /// навигационное свойство
        /// </summary>
        public ICollection<NotebookDto>? NotebooksDto { get; set; }
        /// <summary>
        /// навигационное свойство
        /// </summary>
        public ICollection<PhoneDto>? PhonesDto { get; set; }*/
    }
}

