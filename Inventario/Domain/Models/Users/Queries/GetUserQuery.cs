﻿using Domain.Models.Users;
using MediatR;

namespace Domain.Users.Queries;

/// <summary>
/// CQRS модель получения пользователя 
/// </summary>
public class GetUserQuery : IRequest<UserDto>
{
    /// <summary>
    /// Идентификатор пользователя
    /// </summary>
    public int Id { get; set; }
}
