﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using MediatR;

namespace Domain.Models.Modems.Commands
{
    /// <summary>
    /// CQRS обработчик создания модема
    /// </summary>
    public class CreateModemCommandHandler : IRequestHandler<CreateModemCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public CreateModemCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды создания модема
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор нового модема</returns>
        public async Task<int> Handle(CreateModemCommand request, CancellationToken cancellationToken)
        {
            var modem = _mapper.Map<Modem>(request);
            await _dataContext.Modems.AddAsync(modem, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return modem.ID;
        }
    }
}
