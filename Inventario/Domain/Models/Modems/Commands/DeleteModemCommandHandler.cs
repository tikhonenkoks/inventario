﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;

namespace Domain.Models.Modems.Commands
{
    /// <summary>
    /// CQRS обработчик удаления модема
    /// </summary>
    public class DeleteModemCommandHandler : IRequestHandler<DeleteModemCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public DeleteModemCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды удаления модема
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор удаляемого модема</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого модема нет</exception>
        public async Task<int> Handle(DeleteModemCommand request, CancellationToken cancellationToken)
        {
            var modem = _dataContext.Modems
            .FirstOrDefault(modem1 => modem1.ID == request.Id);
            if (modem != null)
            {
                _dataContext.Modems.Remove(modem);
                await _dataContext.SaveChangesAsync(cancellationToken);
                return modem.ID;
            }
            throw new NotFoundException($"Модем с Id: {request.Id} не найден");
        }
    }
}
