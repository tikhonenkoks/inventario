﻿using MediatR;

namespace Domain.Models.Employees.Commands
{
    /// <summary>
    /// CQRS модель удаления сотрудника 
    /// </summary>
    public class DeleteEmployeeCommand : IRequest<int>
    {
        /// <summary>
        /// Идентификатор сотрудника
        /// </summary>
        public int Id { get; set; }
    }
}
