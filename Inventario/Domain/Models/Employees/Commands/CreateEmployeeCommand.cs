﻿using Domain.Models.Modems;
using MediatR;

namespace Domain.Models.Employees.Commands
{
    /// <summary>
    /// CQRS модель создания сотрудника 
    /// </summary>
    public class CreateEmployeeCommand : IRequest<int>
    {
        /// <summary>
        /// Идентификатор сотрудника
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Отдел
        /// </summary>
        public string Department { get; set; }

        /*/// <summary>
        /// навигационное свойство
        /// </summary>
        public ICollection<ModemDto>? ModemsDto { get; set; }
        /// <summary>
        /// навигационное свойство
        /// </summary>
        public ICollection<ModemDto>? NotebooksDto { get; set; }
        /// <summary>
        /// навигационное свойство
        /// </summary>
        public ICollection<ModemDto>? PhonesDto { get; set; }*/
    }
}
