﻿using Domain.Models.Users;
using Domain.Users.Commands;
using Domain.Users.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RouteAttribute = Microsoft.AspNetCore.Mvc.RouteAttribute;

namespace Controllers;

[Authorize]
/// <summary>
/// Контроллер для UserDto
/// </summary>
[ApiController]
[Route("api/[controller]")]

public class UserController : ControllerBase
{
    private readonly IMediator _mediator;
    /// <summary>
    /// Конструктор для контроллера
    /// </summary>
    /// <param name="mediator"></param>
    public UserController(IMediator mediator) // в конструкторе прописана зависимость
    {
        _mediator = mediator;
    }

    [Authorize(Roles = "Admin")]
    /// <summary>
    /// Получение всех пользователей
    /// </summary>
    /// <returns>Результирующий набор пользователей</returns>
    [HttpGet]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(IEnumerable<UserDto>), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult<IEnumerable<UserDto>>> GetAll([FromQuery] GetUsersQuery getUsersQuery)
    {
        var usersDto = await _mediator.Send(getUsersQuery);
        return Ok(usersDto);
    }

    /// <summary>
    /// Создание пользователя
    /// </summary>
    /// <param name="createUserCommand">Экземпляр CreateUserCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Create([FromBody] CreateUserCommand createUserCommand)
    {
        await _mediator.Send(createUserCommand);
        return Ok();
    }

    /// <summary>
    /// Удаление пользователя по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации
    public async Task<ActionResult> Delete([FromRoute] int id)
    {
        var userDto = new DeleteUserCommand() { Id = id };
        await _mediator.Send(userDto);
        return Ok(userDto);
    }

    /// <summary>
    /// Изменение пользователя по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <param name="editUserCommand">Экземпляр EditUserCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPut]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Edit([FromBody] EditUserCommand editUserCommand)
    {
        var user = await _mediator.Send(editUserCommand);
        return Ok(user);
    }
}

