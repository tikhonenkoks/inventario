﻿using MediatR;

namespace Domain.Models.Employees.Queries;

/// <summary>
/// CQRS модель получения сотрудника 
/// </summary>
public class GetEmployeeQuery : IRequest<EmployeeDto>
{
    /// <summary>
    /// Идентификатор пользователя
    /// </summary>
    public int Id { get; set; }
}
