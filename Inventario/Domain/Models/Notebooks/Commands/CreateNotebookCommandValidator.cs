﻿using FluentValidation;

namespace Domain.Models.Notebooks.Commands;

/// <summary>
/// Валидатор CQRS модели создания ноутбука
/// </summary>
public class CreateNotebookCommandValidator : AbstractValidator<NotebookDto>
{
    /// <summary>
    /// Правила валидации CQRS модели создания ноутбука
    /// </summary>
    public CreateNotebookCommandValidator()
    {
        RuleFor(notebook => notebook.ID)
        .NotNull()
        .GreaterThanOrEqualTo(0)
        .WithMessage("Поле должно быть больше или равно 0");

        RuleFor(notebook => notebook.Model)
        .NotNull()
        .MinimumLength(1)
        .MaximumLength(100)
        .WithMessage("Поле Model должно быть от 1 до 100 символов");

        RuleFor(notebook => notebook.SN)
        .NotNull()
        .MinimumLength(1)
        .MaximumLength(100)
        .WithMessage("Поле SN должно содержать от 1 и до 100");

        RuleFor(notebook => notebook.Status)
        .NotNull()
        .WithMessage("Статусы: выдан, готов к выдаче, на списании, списан, в ремонте");

        RuleFor(notebook => notebook.HasLaptopBag)
        .NotNull()
        .WithMessage("Поле HasLaptopBag должно содержать true или false");

        RuleFor(notebook => notebook.HasСomputerMouse)
        .NotNull()
        .WithMessage("Поле HasСomputerMouse должно содержать true или false");

        RuleFor(notebook => notebook.DateOfIssue)
        .NotNull()
        .WithMessage("Поле DateOfIssue должно быть в формате даты yyyy.MM.dd");

    }
}

