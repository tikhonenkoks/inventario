﻿using FluentValidation;

namespace Domain.Users.Queries
{
    /// <summary>
    /// Валидатор CQRS модели получения пользователя
    /// </summary>
    public class GetUserQueryValidator : AbstractValidator<GetUserQuery>
    {
        /// <summary>
        /// Правила валидации CQRS модели получения пользователя
        /// </summary>
        public GetUserQueryValidator()
        {
            RuleFor(query => query.Id)
                .NotNull()
                .NotEmpty()
                .GreaterThanOrEqualTo(0)
                .WithMessage("Поле должно быть больше или равно 0");
        }
    }
}
