﻿using Domain.Models.Users;
using MediatR;

namespace Domain.Users.Commands
{
    /// <summary>
    /// CQRS модель создания пользователя 
    /// </summary>
    public class CreateUserCommand : IRequest<int>
    {
        /// <summary>
        /// Идентификатор сотрудника
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Роль и права доступа
        /// </summary>
        public Role Role { get; set; }
    }
}
