﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Phones.Queries;

/// <summary>
/// CQRS обработчик получения телефона
/// </summary>
public class GetPhoneQueryHandler : IRequestHandler<GetPhoneQuery, PhoneDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Констуктор для добавления зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetPhoneQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик запроса получения телефона
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Телефон</returns>
    /// <exception cref="NotFoundException">Обработчик исключений : такого телефона нет</exception>
    public async Task<PhoneDto> Handle(GetPhoneQuery request, CancellationToken cancellationToken)
    {
        var phone = await _dataContext.Phones
            .AsNoTracking() //отключение отслеживания изменений
            .FirstOrDefaultAsync(phone1 => phone1.ID == request.Id);
        if (phone == null)
            throw new NotFoundException($"Билет с id={request.Id} не найден");
        return _mapper.Map<PhoneDto>(phone);
    }
}
