﻿using Domain.Models.Notebooks;
using Domain.Models.Notebooks.Commands;
using Domain.Models.Notebooks.Queries;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Controllers;

[Authorize]
/// <summary>
/// Контроллер для NotebookDto
/// </summary>
[ApiController]
[Route("api/[controller]/[action]")]
public class NotebookController : ControllerBase
{
    private readonly IMediator _mediator;

    /// <summary>
    /// Конструктор для контроллера
    /// </summary>
    /// <param name="mediator">медиатор для работы с CQRS</param>
    public NotebookController(IMediator mediator)
    {
        _mediator = mediator;
    }

    /// <summary>
    /// Получение ноутбука по id 
    /// </summary>
    /// <param name="id">Идентификатор ноутбука</param>
    /// <returns>Ноутбук</returns>
    [Authorize(Roles = "Admin, User")]
    [HttpGet("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(NotebookDto), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Get(int id)
    {
        var getNotebookQuery = new GetNotebookQuery() { Id = id };
        var notebookDto = await _mediator.Send(getNotebookQuery);
        return Ok(notebookDto);
    }

    /// <summary>
    /// Получение ноутбуков
    /// </summary>
    /// <param name="getNotebooksQuery"> пагинация</param>
    /// <returns> Результирующий набор ноутбуков </returns>
    [Authorize(Roles = "Admin, User")]
    [HttpGet]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(typeof(IEnumerable<NotebookDto>), StatusCodes.Status200OK)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult<IEnumerable<NotebookDto>>> Get([FromQuery] GetNotebooksQuery getNotebooksQuery)
    {
        var notebooksDto = await _mediator.Send(getNotebooksQuery);
        return Ok(notebooksDto);
    }

    /// <summary>
    /// Создание ноутбука
    /// </summary>
    /// <param name="createNotebookCommand">Экземпляр команды CQRS CreateNotebookCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPost]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации

    public async Task<ActionResult> Create([FromBody] CreateNotebookCommand createNotebookCommand)
    {
        await _mediator.Send(createNotebookCommand);
        return Ok();
    }

    /// <summary>
    /// Удаление ноутбука по идентификатору
    /// </summary>
    /// <param name="id">Идентификатор</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpDelete("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации
    public async Task<ActionResult> Delete([FromRoute] int id)
    {
        var notebook = new DeleteNotebookCommand() { Id = id };
        await _mediator.Send(notebook);
        return Ok(notebook);
    }

    /// <summary>
    /// Изменение ноутбука
    /// </summary>
    /// <param name="editNotebookCommand">Экземпляр команды CQRS editNotebookCommand</param>
    /// <returns>Результат операции</returns>
    [Authorize(Roles = "Admin")]
    [HttpPut("{id}")]
    [Produces("application/json")] // изменили тип медиа данных по умолчнию
    [ProducesResponseType(StatusCodes.Status200OK)] // добавили тип ответа в документации
    [ProducesResponseType(typeof(BadRequestResult), StatusCodes.Status400BadRequest)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(typeof(NotFoundResult), StatusCodes.Status404NotFound)] // добавили тип ответа в документации с указанием типа возвращаемого объекта
    [ProducesResponseType(StatusCodes.Status500InternalServerError)] // добавили тип ответа в документации
    public async Task<ActionResult> Edit([FromBody] EditNotebookCommand editNotebookCommand)
    {
        var notebook = await _mediator.Send(editNotebookCommand);
        return Ok(notebook);
    }

    
}