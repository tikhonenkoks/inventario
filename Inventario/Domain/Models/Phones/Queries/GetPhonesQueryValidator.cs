﻿using FluentValidation;

namespace Domain.Models.Phones.Queries;

/// <summary>
/// Валидатор CQRS модели получения телефонов
/// </summary>
public class GetPhonesQueryValidator : AbstractValidator<GetPhonesQuery>
{
    /// <summary>
    /// Правила валидации CQRS модели получения телефонов
    /// </summary>
    public GetPhonesQueryValidator()
    {
        RuleFor(query => query.Skip).GreaterThanOrEqualTo(0);
        RuleFor(query => query.Take).GreaterThanOrEqualTo(0);

        RuleFor(query => query.Skip).Must((query, skip) => skip <= query.Take);
    }
}
