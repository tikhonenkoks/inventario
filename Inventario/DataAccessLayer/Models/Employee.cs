﻿namespace DataAccessLayer.Models;


public class Employee
{
    public int ID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Department { get; set; }

    public ICollection<Modem> Modems { get; set; } // навигационное свойство
    public ICollection<Notebook> Notebooks { get; set; } // навигационное свойство
    public ICollection<Phone> Phones { get; set; } // навигационное свойство
}
