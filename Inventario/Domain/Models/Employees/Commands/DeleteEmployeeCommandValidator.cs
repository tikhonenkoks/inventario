﻿using FluentValidation;

namespace Domain.Models.Employees.Commands
{

    /// <summary>
    /// Валидатор CQRS модели удаления сотрудника
    /// </summary>
    public class DeleteEmployeeCommandValidator : AbstractValidator<DeleteEmployeeCommand>
    {
        /// <summary>
        /// Правила валидации CQRS модели удаления сотрудника
        /// </summary>
        public DeleteEmployeeCommandValidator()
        {
            RuleFor(command => command.Id)
                .NotNull()
                .GreaterThanOrEqualTo(0)
                .WithMessage("Поле должно быть больше или равно 0");
        }
    }
}