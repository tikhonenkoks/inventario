﻿using MediatR;

namespace Domain.Models.Modems.Commands
{
    /// <summary>
    /// CQRS модель удаления билета 
    /// </summary>
    public class DeleteModemCommand : IRequest<int>
    {
        /// <summary>
        /// Идентификатор модема
        /// </summary>
        public int Id { get; set; }
    }
}
