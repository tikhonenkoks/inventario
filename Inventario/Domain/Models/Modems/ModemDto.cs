﻿using Domain.Models.Employees;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models.Modems;
/// <summary>
/// Модем 
/// </summary>
public class ModemDto
{
    /// <summary>
    /// Идентификатор модема
    /// </summary>
    public int ID { get; set; }
    /// <summary>
    /// Модель модема
    /// </summary>
    public string Model { get; set; }
    /// <summary>
    /// Серийный номер
    /// </summary>
    public string SN { get; set; }
    /// <summary>
    /// Статус оборудования
    /// </summary>
    public Status Status { get; set; }
    /// <summary>
    /// Номер сим-карты
    /// </summary>
    public string PhoneNumber { get; set; }
    /// <summary>
    /// Дата выдачи оборудования
    /// </summary>
    public string DateOfIssue { get; set; }

    /// <summary>
    /// внешний ключ
    /// </summary>
    public int EmployeeDtoID { get; set; }
}
