﻿namespace Domain.Models.Users;

/// <summary>
/// Роли
/// </summary>
public enum Role
{
    Admin = 1,
    User = 2
}