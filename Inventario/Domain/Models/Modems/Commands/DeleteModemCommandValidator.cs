﻿using FluentValidation;

namespace Domain.Models.Modems.Commands
{

    /// <summary>
    /// Валидатор CQRS модели удаления модема
    /// </summary>
    public class DeleteModemCommandValidator : AbstractValidator<DeleteModemCommand>
    {
        /// <summary>
        /// Правила валидации CQRS модели удаления модема
        /// </summary>
        public DeleteModemCommandValidator()
        {
            RuleFor(command => command.Id)
                .NotNull()
                .GreaterThanOrEqualTo(0)
                .WithMessage("Поле должно быть больше или равно 0");
        }
    }
}