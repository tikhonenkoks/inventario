﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Phones.Commands
{
    /// <summary>
    /// CQRS обработчик редактирования телефона 
    /// </summary>
    public class EditPhoneCommandHandler : IRequestHandler<EditPhoneCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public EditPhoneCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды редактирования телефона
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор телефона</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого телефона нет</exception>
        public async Task<int> Handle(EditPhoneCommand request, CancellationToken cancellationToken)
        {
            var phone = await _dataContext.Phones
            .FirstOrDefaultAsync(phone1 => phone1.ID == request.ID, cancellationToken: cancellationToken);

            if (phone == null)
                throw new NotFoundException($"Телефон с ID: {request.ID} не найден");

            _mapper.Map(request, phone);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return phone.ID;
        }
    }
}

