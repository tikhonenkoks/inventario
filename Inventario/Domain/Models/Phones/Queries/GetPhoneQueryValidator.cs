﻿using FluentValidation;

namespace Domain.Models.Phones.Queries
{
    /// <summary>
    /// Валидатор CQRS модели получения телефона
    /// </summary>
    public class GetPhoneQueryValidator : AbstractValidator<GetPhoneQuery>
    {
        /// <summary>
        /// Правила валидации CQRS модели получения телефона
        /// </summary>
        public GetPhoneQueryValidator()
        {
            RuleFor(query => query.Id).GreaterThanOrEqualTo(0);
        }
    }
}
