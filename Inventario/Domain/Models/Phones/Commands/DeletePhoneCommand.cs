﻿using MediatR;

namespace Domain.Models.Phones.Commands
{
    /// <summary>
    /// CQRS модель удаления телефона 
    /// </summary>
    public class DeletePhoneCommand : IRequest<int>
    {
        /// <summary>
        /// Идентификатор телефона
        /// </summary>
        public int Id { get; set; }
    }
}
