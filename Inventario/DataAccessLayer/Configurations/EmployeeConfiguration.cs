﻿using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DataAccessLayer.Configurations
{
    // fluent api - набор правил для Employee -вызывается в datacontext
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> modelBuilder)
        {
            modelBuilder.HasKey(employee => employee.ID); // указание на первичный ключ
            /*//modelBuilder.HasMany(с => с.Modems).WithOne(p => p.Employee).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.HasMany(с => с.Notebooks).WithOne(p => p.Employee).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.HasMany(с => с.Phones).WithOne(p => p.Employee).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Property("FirstName").IsRequired();
            modelBuilder.Property("LastName").IsRequired();
            modelBuilder.Property("Department").IsRequired();*/
        }
    }
}
