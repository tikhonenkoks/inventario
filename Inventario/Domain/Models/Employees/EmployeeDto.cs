﻿using Domain.Models.Modems;
using Domain.Models.Notebooks;
using Domain.Models.Phones;

namespace Domain.Models.Employees;

/// <summary>
/// Класс сотрудников 
/// </summary>
public class EmployeeDto
{
    /// <summary>
    /// Идентификатор сотрудника
    /// </summary>
    public int ID { get; set; }
    /// <summary>
    /// Имя
    /// </summary>
    public string FirstName { get; set; }
    /// <summary>
    /// Фамилия
    /// </summary>
    public string LastName { get; set; }
    /// <summary>
    /// Отдел
    /// </summary>
    public string Department { get; set; }

    /// <summary>
    /// навигационное свойство
    /// </summary>
    public ICollection<ModemDto>? ModemsDto { get; set; }
    /// <summary>
    /// навигационное свойство
    /// </summary>
    public ICollection<NotebookDto>? NotebooksDto { get; set; } 
    /// <summary>
    /// навигационное свойство
    /// </summary>
    public ICollection<PhoneDto>? PhonesDto { get; set; } 
}
