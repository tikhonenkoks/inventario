﻿using MediatR;

namespace Domain.Models.Modems.Queries;

/// <summary>
/// CQRS модель получения модема 
/// </summary>
public class GetModemQuery : IRequest<ModemDto>
{
    /// <summary>
    /// Идентификатор модема
    /// </summary>
    public int Id { get; set; }
}
