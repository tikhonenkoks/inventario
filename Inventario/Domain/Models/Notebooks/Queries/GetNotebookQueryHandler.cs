﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Notebooks.Queries;

/// <summary>
/// CQRS обработчик получения ноутбука
/// </summary>
public class GetNotebookQueryHandler : IRequestHandler<GetNotebookQuery, NotebookDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Констуктор для добавления зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetNotebookQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик запроса получения ноутбука
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Ноутбук</returns>
    /// <exception cref="NotFoundException">Обработчик исключений : такого ноутбука нет</exception>
    public async Task<NotebookDto> Handle(GetNotebookQuery request, CancellationToken cancellationToken)
    {
        var notebook = await _dataContext.Notebooks
            .AsNoTracking() //отключение отслеживания изменений
            .FirstOrDefaultAsync(notebook1 => notebook1.ID == request.Id);
        if (notebook == null)
            throw new NotFoundException($"Ноутбук с id={request.Id} не найден");
        return _mapper.Map<NotebookDto>(notebook);
    }
}
