﻿using FluentValidation;

namespace Domain.Models.Employees.Commands;

/// <summary>
/// Валидатор CQRS модели создания сотрудника
/// </summary>
public class CreateEmployeeCommandValidator : AbstractValidator<EmployeeDto>
{
    /// <summary>
    /// Правила валидации CQRS модели создания сотрудника
    /// </summary>
    public CreateEmployeeCommandValidator()
    {
        RuleFor(employee => employee.ID)
            .NotNull()
            .GreaterThanOrEqualTo(0)
            .WithMessage("Поле должно быть больше или равно 0");

        RuleFor(employee => employee.FirstName)
            .NotNull()
            .MinimumLength(2)
            .MaximumLength(100)
            .WithMessage("Поле FirstName должно содержать от 2 до 100 символов");

        RuleFor(employee => employee.LastName)
            .NotNull()
            .MinimumLength(2)
            .MaximumLength(100)
            .WithMessage("Поле LastName должно содержать от 2 до 100 символов");

        RuleFor(employee => employee.Department)
            .NotNull()
            .MinimumLength(2)
            .MaximumLength(100)
            .WithMessage("Поле Department должно содержать от 2 до 100 символов");
    }
}

