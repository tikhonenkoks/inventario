﻿using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Modems.Queries;

/// <summary>
/// CQRS обработчик получения модемов
/// </summary>
public class GetModemsQueryHandler : IRequestHandler<GetModemsQuery, IEnumerable<ModemDto>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Констуктор для добавления зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetModemsQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик запроса получения модемов
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Коллекция модемов</returns>
    public async Task<IEnumerable<ModemDto>> Handle(GetModemsQuery request, CancellationToken cancellationToken)
    {
        var modems = await _dataContext.Modems
            .Skip(request.Skip)
            .Take(request.Take)
            .AsNoTracking() //выключаем отслеживание для более быстрого выполнения запроса
            .ToListAsync(cancellationToken: cancellationToken);

        return _mapper.Map<IEnumerable<ModemDto>>(modems);

    }
}
