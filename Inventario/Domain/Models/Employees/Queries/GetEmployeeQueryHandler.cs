﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Employees.Queries;

/// <summary>
/// CQRS обработчик получения сотрудника
/// </summary>
public class GetEmployeeQueryHandler : IRequestHandler<GetEmployeeQuery, EmployeeDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Констуктор для добавления зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetEmployeeQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик запроса получения сотрудника
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Сотрудник</returns>
    /// <exception cref="NotFoundException">Обработчик исключений : такого сотрудника нет</exception>
    public async Task<EmployeeDto> Handle(GetEmployeeQuery request, CancellationToken cancellationToken)
    {
        var employee = await _dataContext.Employees
            .AsNoTracking() //отключение отслеживания изменений
            .FirstOrDefaultAsync(employee1 => employee1.ID == request.Id);
        if (employee == null)
            throw new NotFoundException($"Сотрудник с id={request.Id} не найден");
        return _mapper.Map<EmployeeDto>(employee);
    }
}
