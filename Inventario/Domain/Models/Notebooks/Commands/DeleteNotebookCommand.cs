﻿using MediatR;

namespace Domain.Models.Notebooks.Commands
{
    /// <summary>
    /// CQRS модель удаления ноутбука 
    /// </summary>
    public class DeleteNotebookCommand : IRequest<int>
    {
        /// <summary>
        /// Идентификатор ноутбука
        /// </summary>
        public int Id { get; set; }
    }
}
