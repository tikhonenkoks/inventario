﻿using MediatR;

namespace Domain.Models.Notebooks.Queries;

/// <summary>
/// CQRS модель получения ноутбука 
/// </summary>
public class GetNotebookQuery : IRequest<NotebookDto>
{
    /// <summary>
    /// Идентификатор ноутбука
    /// </summary>
    public int Id { get; set; }
}
