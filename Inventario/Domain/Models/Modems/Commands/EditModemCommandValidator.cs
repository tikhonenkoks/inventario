﻿using FluentValidation;

namespace Domain.Models.Modems.Commands
{
    /// <summary>
    /// Валидатор CQRS модели редактирования модема
    /// </summary>
    public class EditModemCommandValidator : AbstractValidator<ModemDto>
    {
        /// <summary>
        /// Правила валидации CQRS модели редактирования модема
        /// </summary>
        public EditModemCommandValidator()
        {
            RuleFor(modem => modem.ID)
            .NotNull()
            .GreaterThanOrEqualTo(0)
            .WithMessage("Поле должно быть больше или равно 0");

            RuleFor(modem => modem.Model)
            .NotNull()
            .MinimumLength(1)
            .MaximumLength(100)
            .WithMessage("Поле Model должно быть от 1 до 100 символов");

            RuleFor(modem => modem.SN)
            .NotNull()
            .MinimumLength(1)
            .MaximumLength(100)
            .WithMessage("Поле SN должно содержать от 1 и до 100");

            RuleFor(modem => modem.Status)
            .NotNull()
            .WithMessage("Статусы: выдан, готов к выдаче, на списании, списан, в ремонте");

            RuleFor(modem => modem.PhoneNumber)
            .NotNull()
            .MinimumLength(7)
            .MaximumLength(11)
            .WithMessage("Поле PhoneNumber должно содержать от 7 и до 11 цифр");

            RuleFor(modem => modem.DateOfIssue)
            .NotNull()
            .WithMessage("Поле DateOfIssue должно быть в формате даты yyyy.MM.dd");

        }
    }
}

