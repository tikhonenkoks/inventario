﻿namespace DataAccessLayer.Models;

/// <summary>
/// Статус оборудования
/// </summary>
public enum Status
{
    HasUser = 1, // у пользователя
    InDepartment = 2, // в отделе
    ForUtilization = 3, // на списание
    ForRepair = 4 // в ремонте
}