﻿using MediatR;

namespace Domain.Models.Phones.Queries;

/// <summary>
/// CQRS модель получения телефона 
/// </summary>
public class GetPhoneQuery : IRequest<PhoneDto>
{
    /// <summary>
    /// Идентификатор телефона
    /// </summary>
    public int Id { get; set; }
}
