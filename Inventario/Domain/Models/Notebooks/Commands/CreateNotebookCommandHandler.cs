﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using MediatR;

namespace Domain.Models.Notebooks.Commands
{
    /// <summary>
    /// CQRS обработчик создания ноутбука
    /// </summary>
    public class CreateNotebookCommandHandler : IRequestHandler<CreateNotebookCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public CreateNotebookCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды создания ноутбука
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор нового ноутбука</returns>
        public async Task<int> Handle(CreateNotebookCommand request, CancellationToken cancellationToken)
        {
            var notebook = _mapper.Map<Notebook>(request);
            await _dataContext.Notebooks.AddAsync(notebook, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return notebook.ID;
        }
    }
}
