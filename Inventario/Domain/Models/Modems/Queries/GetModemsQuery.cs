﻿using MediatR;

namespace Domain.Models.Modems.Queries;

/// <summary>
/// CQRS модель получения модемов пагинацией
/// </summary>
public class GetModemsQuery : IRequest<IEnumerable<ModemDto>>
{
    /// <summary>
    /// Пропуск модемов с начала списка
    /// </summary>
    public int Skip { get; set; }
    /// <summary>
    /// Количество требуемых модемов 
    /// </summary>
    public int Take { get; set; }
}