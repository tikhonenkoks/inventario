﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Notebooks.Commands
{
    /// <summary>
    /// CQRS обработчик редактирования ноутбука 
    /// </summary>
    public class EditNotebookCommandHandler : IRequestHandler<EditNotebookCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public EditNotebookCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды редактирования ноутбука
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор ноутбука</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого ноутбука нет</exception>
        public async Task<int> Handle(EditNotebookCommand request, CancellationToken cancellationToken)
        {
            var notebook = await _dataContext.Notebooks
            .FirstOrDefaultAsync(notebook1 => notebook1.ID == request.ID, cancellationToken: cancellationToken);

            if (notebook == null)
                throw new NotFoundException($"Ноутбук с ID: {request.ID} не найден");

            _mapper.Map(request, notebook);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return notebook.ID;
        }
    }
}

