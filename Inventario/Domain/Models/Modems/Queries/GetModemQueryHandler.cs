﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Modems.Queries;

/// <summary>
/// CQRS обработчик получения модема
/// </summary>
public class GetModemQueryHandler : IRequestHandler<GetModemQuery, ModemDto>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Констуктор для добавления зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetModemQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик запроса получения модема
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Модем</returns>
    /// <exception cref="NotFoundException">Обработчик исключений : такого модема нет</exception>
    public async Task<ModemDto> Handle(GetModemQuery request, CancellationToken cancellationToken)
    {
        var modem = await _dataContext.Modems
            .AsNoTracking() //отключение отслеживания изменений
            .FirstOrDefaultAsync(modem1 => modem1.ID == request.Id);
        if (modem == null)
            throw new NotFoundException($"Модем с id={request.Id} не найден");
        return _mapper.Map<ModemDto>(modem);
    }
}
