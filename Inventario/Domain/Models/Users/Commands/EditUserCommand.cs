﻿using Domain.Models.Users;
using MediatR;


namespace Domain.Users.Commands
{
    /// <summary>
    /// CQRS модель редактирования пользователя
    /// </summary>
    public class EditUserCommand : IRequest<int>
    {
        /// <summary>
        /// Идентификатор сотрудника
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// Роль и права доступа
        /// </summary>
        public Role Role { get; set; }
    }
}

