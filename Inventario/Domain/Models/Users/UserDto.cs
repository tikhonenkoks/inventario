﻿namespace Domain.Models.Users;

/// <summary>
/// Класс сотрудников 
/// </summary>
public class UserDto
{
    /// <summary>
    /// Идентификатор сотрудника
    /// </summary>
    public int ID { get; set; }
    /// <summary>
    /// Логин
    /// </summary>
    public string Login { get; set; }
    /// <summary>
    /// Пароль
    /// </summary>
    public string Password { get; set; }
    /// <summary>
    /// Роль и права доступа
    /// </summary>
    public Role Role { get; set; }
}
