﻿using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Notebooks.Queries;

/// <summary>
/// CQRS обработчик получения ноутбуков
/// </summary>
public class GetNotebooksQueryHandler : IRequestHandler<GetNotebooksQuery, IEnumerable<NotebookDto>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Констуктор для добавления зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetNotebooksQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик запроса получения ноутбуков
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Коллекция ноутбуков</returns>
    public async Task<IEnumerable<NotebookDto>> Handle(GetNotebooksQuery request, CancellationToken cancellationToken)
    {
        var notebookы = await _dataContext.Notebooks
            .Skip(request.Skip)
            .Take(request.Take)
            .AsNoTracking() //выключаем отслеживание для более быстрого выполнения запроса
            .ToListAsync(cancellationToken: cancellationToken);

        return _mapper.Map<IEnumerable<NotebookDto>>(notebookы);

    }
}
