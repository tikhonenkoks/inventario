﻿using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Phones.Queries;

/// <summary>
/// CQRS обработчик получения телефонов
/// </summary>
public class GetPhonesQueryHandler : IRequestHandler<GetPhonesQuery, IEnumerable<PhoneDto>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Констуктор для добавления зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetPhonesQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик запроса получения телефонов
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Коллекция телефонов</returns>
    public async Task<IEnumerable<PhoneDto>> Handle(GetPhonesQuery request, CancellationToken cancellationToken)
    {
        var phones = await _dataContext.Phones
            .Skip(request.Skip)
            .Take(request.Take)
            .AsNoTracking() //выключаем отслеживание для более быстрого выполнения запроса
            .ToListAsync(cancellationToken: cancellationToken);

        return _mapper.Map<IEnumerable<PhoneDto>>(phones);

    }
}
