﻿using AutoMapper;
using DataAccessLayer;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Employees.Queries;

/// <summary>
/// CQRS обработчик получения всех сотрудников
/// </summary>
public class GetEmployeesQueryHandler : IRequestHandler<GetEmployeesQuery, IEnumerable<EmployeeDto>>
{
    private readonly DataContext _dataContext;
    private readonly IMapper _mapper;

    /// <summary>
    /// Констуктор для добавления зависимостей от базы данных и автомаппера
    /// </summary>
    /// <param name="dataContext"></param>
    /// <param name="mapper"></param>
    public GetEmployeesQueryHandler(DataContext dataContext, IMapper mapper)
    {
        _dataContext = dataContext;
        _mapper = mapper;
    }

    /// <summary>
    /// CQRS обработчик запроса получения всех сотрудников
    /// </summary>
    /// <param name="request">тело запроса</param>
    /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
    /// <returns>Коллекция сотрудников</returns>
    public async Task<IEnumerable<EmployeeDto>> Handle(GetEmployeesQuery request, CancellationToken cancellationToken)
    {
        var employees = await _dataContext.Employees
            .AsNoTracking()//выключаем отслеживание для более быстрого выполнения запроса
            .ToListAsync(cancellationToken: cancellationToken);
        return _mapper.Map<IEnumerable<EmployeeDto>>(employees);
    }
}
