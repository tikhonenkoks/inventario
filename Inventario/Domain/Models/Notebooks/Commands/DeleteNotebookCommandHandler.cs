﻿using AutoMapper;
using DataAccessLayer;
using Domain.Exceptions;
using MediatR;

namespace Domain.Models.Notebooks.Commands
{
    /// <summary>
    /// CQRS обработчик удаления ноутбука
    /// </summary>
    public class DeleteNotebookCommandHandler : IRequestHandler<DeleteNotebookCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public DeleteNotebookCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды удаления ноутбука
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор удаляемого ноутбука</returns>
        /// <exception cref="NotFoundException">Обработчик исключений : такого ноутбука нет</exception>
        public async Task<int> Handle(DeleteNotebookCommand request, CancellationToken cancellationToken)
        {
            var notebook = _dataContext.Notebooks
            .FirstOrDefault(notebook1 => notebook1.ID == request.Id);
            if (notebook != null)
            {
                _dataContext.Notebooks.Remove(notebook);
                await _dataContext.SaveChangesAsync(cancellationToken);
                return notebook.ID;
            }
            throw new NotFoundException($"Ноутбук с Id: {request.Id} не найден");
        }
    }
}
