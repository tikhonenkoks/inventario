﻿using AutoMapper;
using DataAccessLayer.Models;
using Domain.Users.Commands;

namespace Domain.Models.Users;

/// <summary>
/// Автомаппер CQRS модели пользователя и модели в базе данных
/// </summary>
public class UserMapConfig : Profile
{
    /// <summary>
    /// Правила маппинга CQRS модели пользователя и модели в базе данных
    /// </summary>
    public UserMapConfig()
    {
        CreateMap<UserDto, User>()
            .ForMember(user => user.ID, expression => expression.MapFrom(dto => dto.ID))
            .ForMember(user => user.Login, expression => expression.MapFrom(dto => dto.Login))
            .ForMember(user => user.Password, expression => expression.MapFrom(dto => dto.Password))
            .ForMember(user => user.Role, expression => expression.MapFrom(dto => dto.Role))
            .ReverseMap();

        CreateMap<CreateUserCommand, User>()
            .ForMember(user => user.ID, expression => expression.MapFrom(create => create.ID))
            .ForMember(user => user.Login, expression => expression.MapFrom(create => create.Login))
            .ForMember(user => user.Password, expression => expression.MapFrom(create => create.Password))
            .ForMember(user => user.Role, expression => expression.MapFrom(create => create.Role))
            .ReverseMap();

        CreateMap<DeleteUserCommand, User>()
            .ForMember(user => user.ID, expression => expression.MapFrom(delete => delete.Id))
            .ReverseMap();

        CreateMap<EditUserCommand, User>()
            .ForMember(user => user.ID, expression => expression.MapFrom(edit => edit.ID))
            .ForMember(user => user.Login, expression => expression.MapFrom(edit => edit.Login))
            .ForMember(user => user.Password, expression => expression.MapFrom(edit => edit.Password))
            .ForMember(user => user.Role, expression => expression.MapFrom(edit => edit.Role))
            .ReverseMap();
    }
}
