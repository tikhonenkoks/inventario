﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Users.Commands
{
    /// <summary>
    /// CQRS обработчик создания пользователя
    /// </summary>
    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public CreateUserCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды создания пользователя
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор нового пользователя</returns>
        public async Task<int> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var user = _mapper.Map<User>(request);
            await _dataContext.Users.AddAsync(user, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return user.ID;
        }
    }
}
