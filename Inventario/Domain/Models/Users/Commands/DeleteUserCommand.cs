﻿using MediatR;

namespace Domain.Users.Commands
{
    /// <summary>
    /// CQRS модель удаления пользователя 
    /// </summary>
    public class DeleteUserCommand : IRequest<int>
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        public int Id { get; set; }
    }
}
