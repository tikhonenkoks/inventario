﻿using FluentValidation;

namespace Domain.Models.Notebooks.Queries
{
    /// <summary>
    /// Валидатор CQRS модели получения ноутбука
    /// </summary>
    public class GetNotebookQueryValidator : AbstractValidator<GetNotebookQuery>
    {
        /// <summary>
        /// Правила валидации CQRS модели получения ноутбука
        /// </summary>
        public GetNotebookQueryValidator()
        {
            RuleFor(query => query.Id).GreaterThanOrEqualTo(0);
        }
    }
}
