﻿using DataAccessLayer.Configurations;
using DataAccessLayer.Models;
using Microsoft.EntityFrameworkCore;

namespace DataAccessLayer
{
    public class DataContext: DbContext
    {
        public DbSet<User> Users { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Modem> Modems { get; set; }

        public DbSet<Notebook> Notebooks { get; set; }

        public DbSet<Phone> Phones { get; set; }


        public DataContext(DbContextOptions<DataContext> options): base(options)
        {
            Database.Migrate();// обновление БД при запуске приложения
        }

        // fluent api - описываются взаимосвязи и конфигурации моделей и таблиц
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EmployeeConfiguration).Assembly); // добавление конфигурации в modelBuilder
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ModemConfiguration).Assembly); // добавление конфигурации в modelBuilder
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(NotebookConfiguration).Assembly); // добавление конфигурации в modelBuilder
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(PhoneConfiguration).Assembly); // добавление конфигурации в modelBuilder
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(UserConfiguration).Assembly); // добавление конфигурации в modelBuilder
        }
    }
}
