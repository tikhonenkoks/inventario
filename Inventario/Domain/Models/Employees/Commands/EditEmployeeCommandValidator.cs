﻿using FluentValidation;

namespace Domain.Models.Employees.Commands
{
    /// <summary>
    /// Валидатор CQRS модели редактирования сотрудника
    /// </summary>
    public class EditEmployeeCommandValidator : AbstractValidator<EmployeeDto>
    {
        /// <summary>
        /// Правила валидации CQRS модели редактирования сотрудника
        /// </summary>
        public EditEmployeeCommandValidator()
        {
            RuleFor(user => user.ID)
                .NotNull()
                .GreaterThanOrEqualTo(0)
                .WithMessage("Поле должно быть больше или равно 0");

            RuleFor(user => user.FirstName)
                .NotNull()
                .MinimumLength(2)
                .MaximumLength(100)
                .WithMessage("Поле FirstName должно содержать от 2 до 100 символов");

            RuleFor(user => user.LastName)
                .NotNull()
                .MinimumLength(2)
                .MaximumLength(100)
                .WithMessage("Поле LastName должно содержать от 2 до 100 символов");

            RuleFor(user => user.Department)
                .NotNull()
                .MinimumLength(2)
                .MaximumLength(100)
                .WithMessage("Поле Department должно содержать от 2 до 100 символов");
        }
    }
}

