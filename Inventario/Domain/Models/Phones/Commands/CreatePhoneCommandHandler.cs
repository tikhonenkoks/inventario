﻿using AutoMapper;
using DataAccessLayer;
using DataAccessLayer.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Domain.Models.Phones.Commands
{
    /// <summary>
    /// CQRS обработчик создания телефона
    /// </summary>
    public class CreatePhoneCommandHandler : IRequestHandler<CreatePhoneCommand, int>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        /// <summary>
        /// Констуктор для добавления зависимостей от базы данных и автомаппера 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="mapper"></param>
        public CreatePhoneCommandHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        /// <summary>
        /// CQRS обработчик команды создания телефона
        /// </summary>
        /// <param name="request">тело запроса</param>
        /// <param name="cancellationToken">токен отмены асинхронного выполнения</param>
        /// <returns>Идентификатор нового телефона</returns>
        public async Task<int> Handle(CreatePhoneCommand request, CancellationToken cancellationToken)
        {
            var phone = _mapper.Map<Phone>(request);
            await _dataContext.Phones.AddAsync(phone, cancellationToken);
            await _dataContext.SaveChangesAsync(cancellationToken);
            return phone.ID;
        }
    }
}
