﻿using FluentValidation;

namespace Domain.Models.Employees.Queries
{
    /// <summary>
    /// Валидатор CQRS модели получения сотрудника
    /// </summary>
    public class GetEmployeeQueryValidator : AbstractValidator<GetEmployeeQuery>
    {
        /// <summary>
        /// Правила валидации CQRS модели получения сотрудника
        /// </summary>
        public GetEmployeeQueryValidator()
        {
            RuleFor(query => query.Id)
                .NotNull()
                .GreaterThanOrEqualTo(0)
                .WithMessage("Поле должно быть больше или равно 0");
        }
    }
}
