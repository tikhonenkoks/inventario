﻿using Domain.Models.Notebooks.Commands;
using FluentValidation;

/// <summary>
/// Валидатор CQRS модели удаления ноутбука
/// </summary>
public class DeleteNotebookCommandValidator : AbstractValidator<DeleteNotebookCommand>
{
    /// <summary>
    /// Правила валидации CQRS модели удаления ноутбука
    /// </summary>
    public DeleteNotebookCommandValidator()
    {
        RuleFor(command => command.Id)
        .NotNull()
        .GreaterThanOrEqualTo(0)
        .WithMessage("Поле должно быть больше или равно 0");
    }
}