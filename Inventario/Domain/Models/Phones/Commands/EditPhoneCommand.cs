﻿using Domain.Models.Employees;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace Domain.Models.Phones.Commands
{
    /// <summary>
    /// CQRS модель редактирования телефона
    /// </summary>
    public class EditPhoneCommand : IRequest<int>
    {
        /// <summary>
        /// Идентификатор телефона
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Модель телефона
        /// </summary>
        public string Model { get; set; }
        /// <summary>
        /// Жанр
        /// </summary>
        public string SN { get; set; }
        /// <summary>
        /// Статус оборудования
        /// </summary>
        public Status Status { get; set; }
        /// <summary>
        /// В комплекте с зарядным устройством
        /// </summary>
        public bool HasCharger { get; set; }
        /// <summary>
        /// Номер сим-карты
        /// </summary>
        public string PhoneNumber { get; set; }
        /// <summary>
        /// Дата выдачи оборудования
        /// </summary>
        public string DateOfIssue { get; set; }

    }
}

